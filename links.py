"""
Данный файл предназначен для создания новых коротких url через cli
TODO Необходимо реализовать проверку на дублирование url адресо
TODO Необходимо реализовать вывод справочной информации
"""
import redis
import yaml
import sys
import string
import random

N = 8

with open("config.yaml") as f:
    config = yaml.load(f)
r = redis.Redis(host=config['redis']['host'], port=int(config['redis']['port']), db=int(config['redis']['db']))

print(sys.argv)

if sys.argv[1] and sys.argv[1] == 'add':
    url = sys.argv[2]
    key = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(N))
    while r.get(key):
        key = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(N))
    r.set(key, url)
    print("Short url - {0}\n Full url - {1}".format(key, url))
