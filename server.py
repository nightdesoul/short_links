import tornado.ioloop
import tornado.web
import redis
import yaml

r = None
cache = {}


class MainHandler(tornado.web.RequestHandler):
    def get(self, short_link):
        # See cache in memory
        url = cache.get(short_link)
        if url:
            self.redirect(url)
            return
        # Get data from redis
        url = r.get(short_link)
        if url:
            cache.update({short_link: url})
            self.redirect(url)
        else:
            self.set_status(404)
            self.finish("404")


def make_app():
    return tornado.web.Application([
        (r"/(.*)", MainHandler),
    ])


if __name__ == "__main__":
    config = None
    with open("config.yaml") as f:
        config = yaml.load(f)
    r = redis.Redis(host=config['redis']['host'], port=int(config['redis']['port']), db=int(config['redis']['db']))
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()